package main

import (
	"fmt"
	"log"

	"github.com/gorilla/websocket"
)

func main() {
	// Membuat koneksi WebSocket ke server
	conn, _, err := websocket.DefaultDialer.Dial("ws://localhost:8080/echo", nil)
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	// Mengirim pesan ke server
	message := []byte("Hello, server!")
	err = conn.WriteMessage(websocket.TextMessage, message)
	if err != nil {
		log.Println(err)
		return
	}

	// Menerima balasan dari server
	_, receivedMessage, err := conn.ReadMessage()
	if err != nil {
		log.Println(err)
		return
	}

	fmt.Println("Received:", string(receivedMessage))
}
